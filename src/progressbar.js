import React, {Component} from "react";
import PropTypes from "prop-types";
import Styled from "styled-components";

class ProgressBar extends Component{

    constructor(props){
        super(props);
        this.state={progressValue:this.props.value}
        widthProp= this.props.width
        heightProp= this.props.height
        
    
    }
    
    
    componentDidMount() {
        console.log(widthProp);
        
        console.log(heightProp);
        this.timerID = setInterval(
          () => this.update(),
         10
        );
      }
    

      componentWillUnmount() {
        clearInterval(this.timerID);
      }

    update() {    
        this.setState({
          progressValue:this.props.value    
        });
        if(this.props.progressValue< this.props.min){
          this.props.progressValue=this.props.min;
        }
        if(this.props.progressValue> this.props.max){
          this.props.progressValue=this.props.max;
        }

    }

    textCallback(value){
      
      console.log(`${(value/this.props.max)*100}%`)
      if(value){
        
      return `${parseInt((value/this.props.max)*100)}%`;
      }
      else{
        return;
      }
    }

    render(){ 
        return  <Container>
                <progress  min={this.props.min} max={this.props.max} value={this.state.progressValue} text={this.textCallback(this.props.text)} />
               
                </Container>
        }
}
let widthProp;
let heightProp;

ProgressBar.propTypes = {
    value: PropTypes.number.isRequired,
    max: PropTypes.number,
    min: PropTypes.number,
    height: PropTypes.number,
    width: PropTypes.number,

}


ProgressBar.defaultProps = {
    max: 100,
    min: 0,
    widthProp:"auto"
}


const Container = Styled.div`
progress[value]{
    margin-right: 8px;
    background-color:#eee;
    width: ${(props) =>  widthProp}px;
    
    height: ${(props) =>  heightProp}px;
    flex-direction:row;
}
progress:before {
    content: attr(text);
    font-size: ${(props) =>  heightProp/(1.5)}px;
    vertical-align: 0;
    position:absolute;
    left:0;
    right:0;
  }

`;

export default ProgressBar;