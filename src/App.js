import React,{useState, useEffect} from 'react';
import logo from './logo.svg';
import './App.css';

import ProgressBar from "./progressbar";

function App() {
  const [value,setValue]= useState(0);
  useEffect(()=> {
    const interval= setInterval(() => {
      setValue((oldValue) => {
        const newValue = oldValue + 1;

        if(newValue === 100){
          clearInterval(interval);
          
        }
        return newValue;
      })
    }, 1000);
  }, []);


  if (value === 100) {
   
    return (
     <div className="App">
      <header className="App-header">
        
        <img src={logo} className="App-logo" alt="logo" />
        
      </header>
    </div>
    )
  }
  else{
  //example with less attributes, uncomment to show  
  /*return (
    <div className="App" >
      <header className="App-header">
      
        <ProgressBar value={value} max={100}  />
        
      </header>
    </div>)
  }*/
//example with all attributes, comment to show first
  return (
    <div className="App" >
      <header className="App-header">
        
          <ProgressBar min={-50}  height = {20} width={300} max={100} value={value} text={value}  />
              
      </header>
    </div>)
  }

}

export default App;
